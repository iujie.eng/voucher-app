const express = require('express');
const app = express(); // Create Express Application on the app variable

app.use(express.json()); // Middleware

// PORT environment variable
const port =  8080;
app.listen(port, () => console.log(`Listening on port ${port}..`));


const {createPool} = require('mysql');
const pool = createPool({
    host: "localhost",
    user: "root",
    password: "password",
    database: "aichat"
})
const THIRTY_DAYS = 60 * 60 * 24 * 30 * 1000;  // 60 seconds * 60 minutes * 24 hours * 30 days * 1000 ms
const WAITING_INTERVAL = 1500; // 1.5 seconds
const MAX_WAITING_TIME_LOOP = (60 * 10 * 1000) / WAITING_INTERVAL;  // 60 seconds * 10 minutes * 1000 ms
let lock_voucher_state = [];

// store all voucher code
let voucher_code_array = [
    'code1', 
    'code2',
    'code3'
]
let client_purchase_transaction = [];
let pending_qualified_customers = [];
let qualified_customers_id = [];

app.get('/api/customerPurchaseTransaction/:id', (req, res) => {
    // Clear purchase transaction
    client_purchase_transaction = [];
    let customer_id = req.params.id;
    let query_statement = `SELECT * from purchase_transaction WHERE customer_id = ${customer_id}`
    pool.query(query_statement, (err, data) => {

        if (err) {
            throw err;
        }
        client_purchase_transaction = data;
        res.send(client_purchase_transaction);
    })
});

app.get('/api/customerEligibility/:id', (req, res) => {

    // Check if customer already get the voucher code before
    let customer_id = req.params.id;
    if (qualified_customers_id.includes(Number(customer_id))) {
        res.send('Voucher redeemed!');
    }
    
    // Get past 30 days transaction
    let thirty_days_purchase = [];
    let current_time = Date.now();
    client_purchase_transaction.forEach(function(t_transaction) {
        if (current_time - t_transaction['transaction_at']  < THIRTY_DAYS) {
            thirty_days_purchase.push(t_transaction);
        }
    })

    // Check if there is 3 purchase within 30 days
    if (thirty_days_purchase.length < 3) {
        res.send('Does not hit min 3 transaction in past 30 days');
    }
    // Check total transaction >=100
    let total_transaction = 0;
    thirty_days_purchase.forEach(function(t_transaction) {
        total_transaction += t_transaction['total_spend'];
    })
    if (total_transaction < 100) {
        res.send('Does not hit min $100 transaction.');
    }

    // Check if still have available voucher code
    if (voucher_code_array.length === 0) {
        res.send('Voucher has been fully redemmed.');

    }

    // Get the voucher out from the list from voucher code
    let voucher_code = voucher_code_array.pop();

    // Put the customer with voucher code in the pending qualified customer list
    pending_qualified_customers.push({
        customer_id: voucher_code
    })

    // set interval to check if the customer had been qualified
    lock_voucher_state.push({
        'customer_id' : customer_id,
        'waiting_loop': 0,
        'intervalID': setInterval(function() {lockVoucher(customer_id, voucher_code)}, WAITING_INTERVAL)
    })
    
});

function lockVoucher(customer_id, voucher_code) {
    let state_index = lock_voucher_state.findIndex((state) => state['customer_id'] === customer_id);

    // Clear Interval if customer had move to qualified list
    if (qualified_customers_id.includes(Number(customer_id))) {
       clearInterval(lock_voucher_state[state_index]['intervalID']);
       lock_voucher_state.splice(state_index,1);
       return;
    }
    // Clear Interval customer's voucher code being unlocked
    if (voucher_code_array.includes(voucher_code)) {
        clearInterval(lock_voucher_state[state_index]['intervalID']);
        lock_voucher_state.splice(state_index,1);
        return;
    }
    // if hit to max timeout, then unlock voucher code and remove customer from pending_qualified_customers 
    if (lock_voucher_state[state_index]['waiting_loop'] > MAX_WAITING_TIME_LOOP) {
        // remove customer from pending_qualified customer list
        let customer_index = pending_qualified_customers.findIndex((customer) => Object.keys(customer) === customer_id);
        pending_qualified_customers.splice(customer_index, 1);
        voucher_code_array.push(voucher_code);
        clearInterval(lock_voucher_state[state_index]['intervalID']);
        lock_voucher_state.splice(state_index,1);
        return;
    }
    return lock_voucher_state[state_index]['waiting_loop'] ++;
}

app.get('/api/validatePhoto/:id', (req, res) => {
    // Assume image API return true
    let image_verified = true;
    
    let customer_id = req.params.id;

    // Check if customer already get the voucher
    if (qualified_customers_id.includes(Number(customer_id))) {
        res.send('Voucher redeemed!');
    }
    // If not inside the pending_qualified list 
    let targeted_customer = pending_qualified_customers.find((customer) => Object.keys(customer) === customer_id);
    if (!targeted_customer) {
        res.send('Not inside pending list');
    }
    if (image_verified) {
        // Move customer from pending_qualified_customers to qualified_customer
        let customer_index = pending_qualified_customers.findIndex((customer) => Object.keys(customer) === customer_id);
        pending_qualified_customers.splice(customer_index, 1);
        qualified_customers_id.push(customer_id);
        // Send code to customer
        res.send(targeted_customer['customer_id']);
    }
    else { //image verify fail
        // remove customer from pending_qualified customer list and add the lock voucher back to voucher_code_array
        let customer_index = pending_qualified_customers.findIndex((customer) => Object.keys(customer) === customer_id);
        pending_qualified_customers.splice(customer_index, 1);
        voucher_code_array.push(voucher_code);
        res.send('Image verified fail');
    }
});


